const signature = await window.crypto.subtle.sign(
  {
      name: "RSA-PSS",
      saltLength: 128,
  },
  keypair.privateKey,
  new Uint8Array(["Ovo je moja poruka!"])
)

const enc = new TextEncoder("utf-8");
const sign = JSON.stringify(enc.encode(signature));