const publicKey = await window.crypto.subtle.importKey(
  "jwk",
  {
    "alg":"PS256",
    "e":"AQAB",
    "ext":true,
    "kty":"RSA","n":"sJV9A16I0HRNd6GyVi91YXAlR7wrcDJnLCAh4wO3Fl_VspAaHEfCOEorjZ2IjOykwEdTJRxl0Yn3t_vMlTfg69oR9GprMw9KCpZ_uihhanRV3tdIA7Z4a-prxRBFxEXK8T6uDkZVwcFJjaDOxRVS2NpS7rp45q92sL8JVcpk-EM3IWaeUXz7WtIHDW-q458nenS-grduSZPmbug03_yRfYk179bqNb5hZjgMkNVQlYtNdgAVhuf4zPfO3w4ZoE4hG_S3tOb7uweN6T7F8b9P6rxAE5wzC-PhSYYWZksQRfa1vSZ7BVkSWyW-nUfx1Ygqtcsb8h3ng_jFmZQbzbIHTw"
  },
  {
      name: "RSA-PSS",
      hash: {name: "SHA-256"},
  },
  false,
  ["verify"]
)