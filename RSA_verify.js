var signature = new Uint8Array(Object.values(JSON.parse('{"0":91,"1":111,"2":98,"3":106,"4":101,"5":99,"6":116,"7":32,"8":65,"9":114,"10":114,"11":97,"12":121,"13":66,"14":117,"15":102,"16":102,"17":101,"18":114,"19":93}'))).buffer;

const isValid = await window.crypto.subtle.verify(
  {
      name: "RSA-PSS",
      saltLength: 128,
  },
  publicKey,
  signature,
  new Uint8Array(["Ovo je moja poruka!"]),
)