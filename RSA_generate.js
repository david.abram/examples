const keypair = await window.crypto.subtle.generateKey(
  {
      name: "RSA-PSS",
      modulusLength: 2048,
      publicExponent: new Uint8Array([0x01, 0x00, 0x01]),
      hash: {name: "SHA-256"},
  },
  false,
  ["sign", "verify"]
)

console.log(keypair);